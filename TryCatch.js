//switch
function whichDayWeek(num){
    
    switch(num){
        case 1:
            return "Monday";
            break;
        case 2:
            return "Tuesday";
            break;
        case 3:
            return "Wednesday";
            break;
        case 4:
            return "Thursday";
            break;
        case 5:
            return "Friday";
            break;
        case 6:
            return "Saturday";
            break;
        case 7:
            return "Sunday";
            break;
        default:
            throw "error: not a day week";
            break;
    }
}
//if
function itsDayWeek(num){
    console.log(`Number: ${num}`);
    if(num>=1&&num<=7){
        console.log(`It's day of week and it's ${whichDayWeek(num)}`);
    }
    else{
        console.log("It's not a day of week");
    }
}


console.log("If and switch example: ");
var num = 4;
itsDayWeek(num);

//for

console.log("\nfor and try-catch example");
var numbers = [1,5,3,9,3,12];
console.log("Array:");
for(i = 0;i<numbers.length;i++){
    console.log(`index: ${i} num: ${numbers[i]}`);
}

console.log("\nIs it day of week?");
for(i of numbers){
    try{
        console.log(`Number: ${i}`);
        whichDayWeek(i);
        console.log(`It's day of week and it's ${whichDayWeek(i)}`)
    }
    catch(e){
        console.error(e);
    }
    
}
